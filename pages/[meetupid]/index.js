
import MeetupDetail from "../../components/meetups/MeetupDetail";

function MeetupDetails(){
    return (<MeetupDetail  
    title='A Second Meetup'
    image='1280px-Englischer_Garten_München.jpg'
    address='Munic city'
    description='this is the Second meetup'/>
    );
}

export default MeetupDetails;