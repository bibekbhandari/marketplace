import MeetupList from '../components/meetups/MeetupList'
import { useState, useEffect } from 'react';

const DUMMY_MEETUPS= [
    {
        id:'m1',
        title:'A First Meetup',
        image:'Schloss_Nymphenburg_Münich.jpg',
        address:'Munic city',
        description:'this is the first meetup'
    },
    {
        id:'m2',
        title:'A Second Meetup',
        image:'1280px-Englischer_Garten_München.jpg',
        address:'Munic city',
        description:'this is the Second meetup'
    }
]

function Hompage(){

    const [LoadedMeetups, setLodedMeetups]= useState([]);

    useEffect(()=>{
        setLodedMeetups(DUMMY_MEETUPS);
    },[]);
    return <MeetupList meetups={LoadedMeetups} /> 
   
}

export default Hompage; 